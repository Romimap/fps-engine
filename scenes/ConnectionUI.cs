using Godot;
using System;

public class ConnectionUI : Control {


    public void Connect () {
        string ip = (GetNode("LineEdit") as LineEdit).Text;
        Lobby.Singleton.Connect(ip, 7777, new Godot.Collections.Dictionary());
        Visible = false;
    }

    public void Localhost () {
        Lobby.Singleton.Connect("LocalHost", 7777, new Godot.Collections.Dictionary());
        Visible = false;
    }

    public void Host () {
        Lobby.Singleton.Host(7777);
        Visible = false;
    }
}
