using Godot;
using System;

public class WeaponSkin : Spatial {
    private PlayerController _playerController;
    private Vector3 _defaultPosition;
    private float _timer;
    private float _speed = 10f;
    private float _amplitude = 0.02f;
    private float _offset = 0.15f;
    private float _slide = 0.1f;
    private Vector3 _currentPosition;
    private Vector3 _targetPosition;

    public override void _Ready () {
        _playerController = (PlayerController)GetParent().GetParent().GetParent().GetParent();
        _defaultPosition = Transform.origin;
        
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        _timer += delta;

        float y = ((Mathf.Sin(_timer * _speed)) * _amplitude * _playerController.CurrentSpeedNormalized) - _offset;
        float x = _playerController.CurrentDirection.x * _slide * _playerController.CurrentSpeedNormalized;

        _targetPosition = _defaultPosition + new Vector3(x, y, 0);

        _currentPosition = _currentPosition * 0.9f + _targetPosition * 0.1f;

        Translation = _currentPosition;
    }
}
