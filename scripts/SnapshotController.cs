using Godot;
using System;

public class SnapshotController : Node {
    //CONFIG
    public static uint SnapshotPeriod = 50;
    public static float SnapshotPeriodF = 0.050f;

    
    private uint _lastSnapshotTime;
    private uint _currentSnapshot = 0;
    public uint CurrentSnapshot {get {return _currentSnapshot;}}
    public static SnapshotController Singleton;

    public override void _Ready() {
        Singleton = this;

        _lastSnapshotTime = OS.GetTicksMsec();
    }
    
    public float timer;
    public int snapshots;
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        if (!GetTree().IsNetworkServer()) return;
        
        uint deltaSnapshotTime = OS.GetTicksMsec() - _lastSnapshotTime;
        if (deltaSnapshotTime >= SnapshotPeriod) {
            _lastSnapshotTime += SnapshotPeriod;
            _currentSnapshot++;
            snapshots++;
            foreach(PlayerController playerController in PlayerController.PlayerControllers) {
                playerController.ServerSideInputHandler.Snapshot();
            }
        }

        timer += delta;
        if (timer > 1) {
            GD.Print("snapshots / seconds" + snapshots);
            timer -= 1;
            snapshots = 0;
        }
    }
}
