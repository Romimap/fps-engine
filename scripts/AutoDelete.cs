using Godot;
using System;

public class AutoDelete : AudioStreamPlayer3D {
    private float _time;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _time = Stream.GetLength();
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (_time < 0) {
            QueueFree();
        }
        _time -= delta;
    }
}
