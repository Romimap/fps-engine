using Godot;
using System;
using System.Collections.Generic;

//Creates input packets and sends them to the server, recieves snapshots
public class ClientSideInputHandler : Node {
    //CONFIG
    private static float _compensationSmoothing = 25f;
    private static float _interpolationSmoothing = 50f;
    public static uint InputPeriod = 30;

    public struct InputData {
        public uint id;
        public uint snapshotID;
        public float delta;
        public float forward;
        public float right;
        public bool jump;
        public bool crouch;
        public bool fire;
        public bool reload;
        public float azimuth;
        public float inclinaison;

        public InputData (Godot.Collections.Array inputDataNetwork) {

            this.id = Convert.ToUInt32(inputDataNetwork[0]);
            this.snapshotID = Convert.ToUInt32(inputDataNetwork[1]);
            this.delta = (float)inputDataNetwork[2];

            this.forward = (float)inputDataNetwork[3];
            this.right = (float)inputDataNetwork[4];
            this.jump = (bool)inputDataNetwork[5];
            this.crouch = (bool)inputDataNetwork[6];

            this.fire = (bool)inputDataNetwork[7];
            this.reload = (bool)inputDataNetwork[8];

            this.azimuth = (float)inputDataNetwork[9];
            this.inclinaison = (float)inputDataNetwork[10];
        }

        public Godot.Collections.Array Serialize () {
            Godot.Collections.Array data = new Godot.Collections.Array();

            data.Add(id);
            data.Add(snapshotID);
            data.Add(delta);
            data.Add(forward);
            data.Add(right);
            data.Add(jump);
            data.Add(crouch);
            data.Add(fire);
            data.Add(reload);
            data.Add(azimuth);
            data.Add(inclinaison);

            return data;
        }

        public static int Compare (InputData x, InputData y) {
            if (x.id > y.id)
                return -1;
            return 1;
        }
    }



    private uint _lastInputTime;
    private uint _currentInput = 0;
    public uint CurrentInput {get {return _currentInput;}}

    private ServerSideInputHandler _serverSideInputHandler;
    private PlayerController _playerController;
    private Weapon _weapon;
    private uint _lastServerInput = 0;
    private uint _lastServerSnapshot = 0;
    private float _lastServerSnapshotTime = 0;
    private InputData _lastInputData;

    private Queue<InputData> _bufferedInputs = new Queue<InputData>();
    private InputData _lastInput;

    //Used for interpolation & smoothing
    private PlayerController.PlayerState _targetPlayerState; 
    private Vector3 _velocityToTargetPlayerState;
    private Vector3 _velocityToPredictionDifference;
    private float _deltaAzimuth;
    private float _deltaInclinaison;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _serverSideInputHandler = (ServerSideInputHandler)GetNode("../ServerSideInputHandler");
        _weapon = (Weapon)GetNode("../Weapon");
        _playerController = (PlayerController)GetParent();
        _lastInputTime = OS.GetTicksMsec();
    }

    public float timer;
    public int inputs;
    public int snapshots;


    public override void _Process(float delta) {
        base._Process(delta);

        if (GetTree().IsNetworkServer()) return;

        if (IsNetworkMaster()) {
            //Input sampling timer
            uint deltaInputTime = OS.GetTicksMsec() - _lastInputTime;
            if (deltaInputTime >= InputPeriod) {
                _lastInputTime += InputPeriod;
                _currentInput++;
                SampleInputs();
            }

            //smoothing
            _playerController.MoveAndSlide((_velocityToTargetPlayerState + _velocityToPredictionDifference) * delta * _compensationSmoothing, new Vector3(0, 1, 0), true, 4, 0.78f, true);
        } else {
            //smoothing
            _playerController.MoveAndSlide(_velocityToTargetPlayerState * delta * _interpolationSmoothing, new Vector3(0, 1 ,0), true, 4, 0.78f, true);
            _playerController.Azimuth += _deltaAzimuth * delta;
            _playerController.Inclinaison += _deltaInclinaison * delta;
        }
    }

    private float _mouseSensitivityX = 0.1f;
    private float _mouseSensitivityY = 0.1f;
    public override void _Input(InputEvent @event) {
        base._Input(@event);
        if (!IsNetworkMaster()) return;

        if (@event is InputEventMouseMotion) {
            InputEventMouseMotion inputEventMouseMotion = (InputEventMouseMotion)@event;
            _playerController.Azimuth -= inputEventMouseMotion.Relative.x * _mouseSensitivityX;
            _playerController.Inclinaison += inputEventMouseMotion.Relative.y * _mouseSensitivityY;
        }

        if (@event is InputEventKey) {
            InputEventKey inputEventKey = (InputEventKey)@event;
            if (inputEventKey.Scancode == (int)KeyList.Escape && inputEventKey.Pressed) {
                if (Input.GetMouseMode() == Input.MouseMode.Visible) {
                    Input.SetMouseMode(Input.MouseMode.Captured);
                } else {
                    Input.SetMouseMode(Input.MouseMode.Visible);
                }
            }
        }
    }

    public void Teleport (Vector3 pos) {
        _playerController.Translation = pos;
        _targetPlayerState = _playerController.GetState();
        _velocityToTargetPlayerState = Vector3.Zero;
        _velocityToPredictionDifference = Vector3.Zero;
    }

    private void SampleInputs () {
        if (_playerController.Dead) return;
        
        inputs++;
        //Storing the current position and set the state to the last known raw state
        Transform transformBackup = _playerController.Transform;
        _playerController.SetState(_targetPlayerState);

        //Sampling inputs
        InputData inputData = new InputData();
        if (Input.IsActionPressed("forward"))
            inputData.forward += 1;
        if (Input.IsActionPressed("back"))
            inputData.forward -= 1;
        if (Input.IsActionPressed("right"))
            inputData.right += 1;
        if (Input.IsActionPressed("left"))
            inputData.right -= 1;
        inputData.crouch = Input.IsActionPressed("crouch");
        inputData.jump = Input.IsActionPressed("jump");
        inputData.fire = Input.IsActionPressed("fire");
        inputData.reload = Input.IsActionPressed("reload");
        inputData.id = CurrentInput;
        inputData.snapshotID = _lastServerSnapshot;
        inputData.delta = (float)(OS.GetTicksMsec() - _lastServerSnapshotTime) / 1000f;
        inputData.azimuth = _playerController.Azimuth;
        inputData.inclinaison = _playerController.Inclinaison;

        _weapon.Step(CurrentInput, inputData.fire, inputData.reload, ClientSideInputHandler.InputPeriod);
        
        //Storing inputs for lag compensation and sending the last one to the server
        _bufferedInputs.Enqueue(inputData);
        _serverSideInputHandler.RpcId(1, "SendInputToServer", CurrentInput, inputData.Serialize());
        //GD.Print("INPUT OUT: " + inputData.Serialize());

        //Moving the player (lag compensation)
        _playerController.Move(inputData, (float)InputPeriod / 1000f);
        _lastInput = inputData;

        //Storing the new raw state and restoring the previously stored position
        _targetPlayerState = _playerController.GetState();
        _playerController.Transform = transformBackup;

        //Computing the velocity needed to get to the new state by the time a new input is sampled
        _velocityToTargetPlayerState = _targetPlayerState.transform.origin - _playerController.Transform.origin;
        _velocityToTargetPlayerState /= ((float)InputPeriod / 1000f); // units per seconds
    }

    [Remote] public void SendSnapshotToClient (uint lastInputID, uint snapshotID, Godot.Collections.Array playerStateNetwork, Godot.Collections.Array weaponStateNetwork) {
        if (GetTree().IsNetworkServer()) return;
        snapshots++;

        if (_lastServerSnapshot > snapshotID) return;

        //GD.Print("SNAPSHOT IN: " + playerStateNetwork);

        //Filling up vars
        _lastServerInput = lastInputID;
        _lastServerSnapshot = snapshotID;
        _lastServerSnapshotTime = OS.GetTicksMsec();
        PlayerController.PlayerState playerState = new PlayerController.PlayerState(playerStateNetwork);
        Weapon.WeaponState weaponState = new Weapon.WeaponState(weaponStateNetwork);

        if (IsNetworkMaster()) { //If we are the master, set the new trusted state then process the remaining inputs (lag compensation)
            //Backing up the position so the player dont teleport
            Transform transformBackup = _playerController.Transform;
            
            //Dequeues the inputs that have been processed by the server
            while (_bufferedInputs.Count > 0 && _bufferedInputs.Peek().id <= lastInputID) {
                _bufferedInputs.Dequeue();
            }

            //Setting the new state and applying the remaining inputs
            _playerController.SetState(playerState);
            _weapon.SetState(weaponState);
            foreach(InputData inputData in _bufferedInputs) {
                _playerController.Move(inputData, (float)InputPeriod / 1000f);
                _weapon.Step(inputData.id, inputData.fire, inputData.reload, (float)InputPeriod / 1000f);
            }

            _targetPlayerState = _playerController.GetState();
            _playerController.Transform = transformBackup;

            //Computing the velocity needed to compensate for prediction error by the time a new snapshot is recieved
            ////GD.Print("prediction error : " + (_targetPlayerState.transform.origin - _playerController.Transform.origin));
            _velocityToPredictionDifference = _targetPlayerState.transform.origin - _playerController.Transform.origin;
            _velocityToPredictionDifference /= ((float)SnapshotController.SnapshotPeriod / 1000f); // units per seconds
        } else { //If we are not the master, fill the interpolation vars
            //Position
            _targetPlayerState = playerState;
            _velocityToTargetPlayerState = _targetPlayerState.transform.origin - _playerController.Transform.origin;
            _velocityToTargetPlayerState /= ((float)SnapshotController.SnapshotPeriod / 1000f); // units per seconds

            //Look
            float currentAzimuth = _playerController.Azimuth;
            float currentInclinaison = _playerController.Inclinaison;
            float targetAzimuth = playerState.azimuth;
            float targetInclinaison = playerState.inclinaison;

            float azimuthA = targetAzimuth - currentAzimuth;
            float azimuthB = targetAzimuth - currentAzimuth - 360f;
            if (Mathf.Abs(azimuthA) < Mathf.Abs(azimuthB)) {
                _deltaAzimuth = azimuthA;
            } else {
                _deltaAzimuth = azimuthB;
            }

            float inclinaisonA = targetInclinaison - currentInclinaison;
            float inclinaisonB = targetInclinaison - currentInclinaison - 360f;
            if (Mathf.Abs(inclinaisonA) < Mathf.Abs(inclinaisonB)) {
                _deltaInclinaison = inclinaisonA;
            } else {
                _deltaInclinaison = inclinaisonB;
            }

            _deltaAzimuth /= ((float)SnapshotController.SnapshotPeriod / 1000f); // degrees per seconds
            _deltaInclinaison /= ((float)SnapshotController.SnapshotPeriod / 1000f); // degrees per seconds

            _playerController._momentum = playerState.momentum; //The momentum is used for animations

            _weapon.SetWeaponStateOthers(weaponState); //Sets the weapon state as if the weapon is handled by someone else than the player
        }
        GD.Print("momentum : " + _playerController._momentum); 
    }
}
