using Godot;
using System;

public class Skin : Spatial {
    Spatial _azimuth;
    ShaderMaterial _shaderMaterial;
    PlayerController _playerController;
    [Export] StreamTexture _idle;
    [Export] StreamTexture _run;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _azimuth = (Spatial)GetNode("../Azimuth");
        _playerController = (PlayerController)GetParent();
        
        (GetNode("Mesh") as MeshInstance).Mesh = (QuadMesh)((GetNode("Mesh") as MeshInstance).Mesh as QuadMesh).Duplicate(true);
        _shaderMaterial = (ShaderMaterial)((GetNode("Mesh") as MeshInstance).Mesh as QuadMesh).Material;
   
        StreamTexture spritesheet = (StreamTexture)ResourceLoader.Load("res://sprites/character/anim.png");
        _shaderMaterial.SetShaderParam("spritesheet", spritesheet);
    }

    float _timer = 0;
    int _frame = 0;
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        if (IsNetworkMaster()) return;

        //Billboard
        if (PlayerCamera.currentCamera != null) {
            LookAt(PlayerCamera.currentCamera.GlobalTransform.origin, new Vector3(0, 1 ,0));
            RotationDegrees = new Vector3(0, RotationDegrees.y, 0);
        }

        //Animation
        float animationSpeed;
        int frame;
        if (_playerController.CurrentSpeedNormalized > 0.1f) {
            animationSpeed = 6 + 4 * _playerController.CurrentSpeedNormalized;
            if (Mathf.Abs(_playerController.CurrentDirection.z) > Mathf.Abs(_playerController.CurrentDirection.x)) {
                if (_playerController.CurrentDirection.z < 0) {
                    frame = _frame + 8;
                } else {
                    frame = (7 - _frame) + 8;
                }
            } else {
                if (_playerController.CurrentDirection.x > 0) {
                    frame = _frame + 16;
                } else {
                    frame = (7 - _frame) + 16;
                }
            }
        } else {
            animationSpeed = 6;
            frame = _frame;
        }

        //Frame
        _timer += delta * animationSpeed;
        if (_timer > 1f) {
            _timer -= 1f;
            _frame++;
            if (_frame > 7) {
                _frame = 0;
            }
        }

        //Orientation
        Vector3 toPlayerCamera = GlobalTransform.origin - PlayerCamera.currentCamera.GlobalTransform.origin;
        toPlayerCamera.y = 0;
        Vector3 forward = -_azimuth.Transform.basis.z;
        forward.y = 0;
        float angle = forward.AngleTo(toPlayerCamera);


        Vector3 left = -_azimuth.Transform.basis.x;
        bool cameraIsLeft = (left.Dot(toPlayerCamera) > 0f);

        if (cameraIsLeft) {
            if (angle < Mathf.Pi / 8) { //0
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 0));
            } else if (angle < (3 * Mathf.Pi) / 8) { //45
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 1));
            } else if (angle < (5 * Mathf.Pi) / 8) { //90
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 2));
            } else if (angle < (7 * Mathf.Pi) / 8) { //135
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 3));
            } else { //180
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 4));
            }
        } else {
            if (angle < Mathf.Pi / 8) { //0
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 0));
            } else if (angle < (3 * Mathf.Pi) / 8) { //45
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 7));
            } else if (angle < (5 * Mathf.Pi) / 8) { //90
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 6));
            } else if (angle < (7 * Mathf.Pi) / 8) { //135
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 5));
            } else { //180
                _shaderMaterial.SetShaderParam("coord", new Vector2(frame, 4));
            }
        }


    }
}
