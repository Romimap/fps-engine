using Godot;
using System;

public class ProjectileController : Node {
    public static ProjectileController Singleton;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        Singleton = this;
    }

    //Spawns the projectile for everyone but the server and the caster
    [Remote] public void Spawn (Vector3 translation, Godot.Collections.Array projectileStateNetwork) {
        Projectile.ProjectileState projectileState = new Projectile.ProjectileState(projectileStateNetwork);
        SceneTree world = Lobby.Singleton.GetTree();
        if (projectileState.casterID == world.GetNetworkUniqueId()) return; //The caster already spawned a projectile.

        Projectile projectile = new Projectile();
        projectile.SetState(projectileState);
        projectile.Name = "P" + projectileState.ID;
        Lobby.Singleton.AddChild(projectile);
        projectile.Translation = translation;
    }
}
