using Godot;
using System;

public class Billboard : MeshInstance {

 // Called every frame. 'delta' is the elapsed time since the previous frame.
 public override void _Process(float delta) {
    LookAt(PlayerCamera.currentCamera.GlobalTransform.origin, Vector3.Up);
 }
}
