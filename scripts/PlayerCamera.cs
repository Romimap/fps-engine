using Godot;
using System;

public class PlayerCamera : Camera {
    public static Camera currentCamera;
    public static Camera defaultCamera;
    private Spatial _weaponInclinaison;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _weaponInclinaison = (Spatial)GetNode("../WeaponInclinaison");
        if (currentCamera == null) {
            
            currentCamera = (Camera)GetNode("/root/World/Camera");
            defaultCamera = (Camera)GetNode("/root/World/Camera");
        }
    }

    private float _deltaXRotation;
    private float _deltaXRotationTimer = 0;
    private float _deltaXRotationPeriod = 0.030f;
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        _deltaXRotationTimer += delta;
        if (_deltaXRotationTimer >= _deltaXRotationPeriod) {
            _deltaXRotationTimer -= _deltaXRotationPeriod;
            processCameraXRotation();
        }

        RotationDegrees += new Vector3(-_deltaXRotation * delta, 0, 0);
    }

    public void processCameraXRotation () {
        float diff = RotationDegrees.x + _weaponInclinaison.RotationDegrees.x;
        _deltaXRotation = diff / 2f;
        _deltaXRotation /= _deltaXRotationPeriod;
        //_deltaXRotation *= 0.0174533f;

    }

    public void SetCurrent () {
        currentCamera = this;
        Current = true;
    }

    public override void _ExitTree() {
        if (currentCamera == this) {
            currentCamera = defaultCamera;
            defaultCamera.Current = true;
        }
        base._ExitTree();
    }
}
