using Godot;
using System;

public class ProjectileSkin : Spatial {
    private Vector3 _speed;
    private Vector3 _offset;
    public PlayerController playerController;

    public override void _Ready() {
        base._Ready();

        _offset = (playerController.GetNode("Azimuth/Inclinaison/Camera/WeaponSkin/ProjectileSkinSpawn") as Spatial).GlobalTransform.origin - GlobalTransform.origin;
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta) {
        GlobalTranslate(_speed * delta + _offset); 
        // _offset -= delta * _offset.Normalized();

        // if (_offset.x < 0) _offset.x = 0;
        // if (_offset.y < 0) _offset.y = 0;
        // if (_offset.z < 0) _offset.z = 0;
    }

    public void SetNewSpeed (Vector3 momentum) {
        _speed = momentum;
    }
}
