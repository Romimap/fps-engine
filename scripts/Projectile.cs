using Godot;
using System;

public class Projectile : Spatial {
    public struct ProjectileState {
        public uint ID;
        public uint casterID;
        public Vector3 momentum;
        public Vector3 changeInMomentumGlobal;
        public Vector3 changeInMomentumLocal;
        public float alive;
        public float speed;

        public ProjectileState (Godot.Collections.Array projectileState) {
            ID = Convert.ToUInt32(projectileState[0]);
            casterID = Convert.ToUInt32(projectileState[1]);
            momentum = (Vector3)projectileState[2];
            changeInMomentumGlobal = (Vector3)projectileState[3];
            changeInMomentumLocal = (Vector3)projectileState[4];
            alive = (float)projectileState[5];
            speed = (float)projectileState[6];
        }

        public Godot.Collections.Array Serialize () {
            Godot.Collections.Array ans = new Godot.Collections.Array();

            ans.Add(ID);
            ans.Add(casterID);
            ans.Add(momentum);
            ans.Add(changeInMomentumGlobal);
            ans.Add(changeInMomentumLocal);
            ans.Add(alive);
            ans.Add(speed);

            return ans;
        }
    }

    private uint _id = 0;
    public uint CasterID;
    private PlayerController _caster;
    private Vector3 _momentum;
    private Vector3 _changeInMomentumGlobal;
    private Vector3 _changeInMomentumLocal;
    private float _alive;
    private float _speed;
    private float _damage = 100;
    private bool _hit = false;
    private string _gunshotSoundScenePath = "res://scenes/GunshotSound.tscn";
    private ProjectileSkin _projectileSkin;


    // Called when the node enters the scene tree for the first time.
    // NOTE: the projectile should be named by the weapon, on both the server and the caster
    // NOTE: the weapon should synchronize and use prediction to name the projectile on the caster side


    public override void _Ready() {
        _caster = (PlayerController)Lobby.Singleton.GetNode("" + CasterID);
        if (_caster==null) QueueFree();

        //Projectile Skin
        _projectileSkin = (ProjectileSkin)(ResourceLoader.Load("res://scenes/Weapons/Projectiles/AWPProjectileSkin.tscn") as PackedScene).Instance();
        _projectileSkin.GlobalTransform = (_caster.GetNode("Azimuth/Inclinaison/Camera") as Spatial).GlobalTransform;
        _projectileSkin.playerController = _caster;
        Lobby.Singleton.AddChild(_projectileSkin);

        Vector3 direction = (_momentum + GlobalTransform.origin) - _projectileSkin.GlobalTransform.origin;
        direction /= _stepPeriod;
        _projectileSkin.SetNewSpeed(direction);

        SetNetworkMaster(1);
        if (GetTree().IsNetworkServer()) {
            //FastForward from client time to server time
            //We retrieve the client time
            uint snapshotId = _caster.ServerSideInputHandler.LastSnapshotProcessedByClient;
            float delta = _caster.ServerSideInputHandler.LastSnapshotProcessedDelta;
            float newDelta = 0;
            //For each snapshot n and n+1 pair
            while (snapshotId < SnapshotController.Singleton.CurrentSnapshot) {
                int difference = (int)snapshotId - (int)SnapshotController.Singleton.CurrentSnapshot; //NOTE: maybe we should substract 1 there
                GD.Print("Fast Forward : " + difference + " (" + snapshotId + " -> " + SnapshotController.Singleton.CurrentSnapshot + ") d " + delta);
                if (difference > 0) {
                    //For each playercontroller
                    foreach (PlayerController playerController in PlayerController.PlayerControllers) {
                        PlayerController.PlayerState playerStateBkp = playerController.GetState();

                        PlayerController.PlayerState playerStateFrom = playerController.ServerSideInputHandler.PlayerStates[playerController.ServerSideInputHandler.PlayerStates.Count - difference - 1];
                        PlayerController.PlayerState playerStateTo = playerController.ServerSideInputHandler.PlayerStates[playerController.ServerSideInputHandler.PlayerStates.Count - difference];
                        float d = delta;
                        //We interpolate between the two snapshots and step the projectile until we reach the next snapshot
                        while (d < SnapshotController.SnapshotPeriodF) {
                            PlayerController.PlayerState interpolatedState = new PlayerController.PlayerState(playerStateFrom, playerStateTo, d / SnapshotController.SnapshotPeriodF);
                            _caster.SetState(interpolatedState);

                            Step(_stepPeriod);

                            d += _stepPeriod;
                        }
                        d += _stepPeriod;
                        d -= SnapshotController.SnapshotPeriodF;
                        newDelta = d;

                        playerController.SetState(playerStateBkp);
                    }
                }
                delta = newDelta;
                snapshotId++;
            }

            //Send the state to everyone
            ProjectileController.Singleton.Rpc("Spawn", Translation, GetState().Serialize());
        }
    }

    public ProjectileState GetState() {
        ProjectileState ans;

        ans.ID = _id;
        ans.casterID = (uint)_caster.ID;
        ans.momentum = _momentum;
        ans.changeInMomentumGlobal = _changeInMomentumGlobal;
        ans.changeInMomentumLocal = _changeInMomentumLocal;
        ans.alive = _alive;
        ans.speed = _speed;

        return ans;
    }

    public void SetState(ProjectileState projectileState) {
        _id = projectileState.ID;
        CasterID = projectileState.casterID;
        _momentum = projectileState.momentum;
        _changeInMomentumGlobal = projectileState.changeInMomentumGlobal;
        _changeInMomentumLocal = projectileState.changeInMomentumLocal;
        _alive = projectileState.alive;
        _speed = projectileState.speed;
    }

    //march the simulation delta seconds
    public void Step (float delta) {
        if (_hit) return;

        PhysicsDirectSpaceState spaceState = PhysicsServer.SpaceGetDirectState(GetWorld().Space);
        Vector3 from = Transform.origin;
        Translation += _momentum;
        Vector3 to = Transform.origin;

        _momentum += _changeInMomentumGlobal * delta;
        //NOTE: not sure about that
        _momentum += _changeInMomentumLocal.x * Transform.basis.x * delta;
        _momentum += _changeInMomentumLocal.y * Transform.basis.y * delta;
        _momentum += _changeInMomentumLocal.z * Transform.basis.z * delta;

        Godot.Collections.Array exclude = new Godot.Collections.Array();
        exclude.Add(_caster);
        
        Godot.Collections.Dictionary result = spaceState.IntersectRay(from, to, exclude);

        if (result.Count > 0) {
            if (GetTree().IsNetworkServer()) {
                Translation = (Vector3)result["position"];
                Rpc("Hit", result["position"], (Vector3)result["normal"]);
                _hit = true;
                RmProjectileSkin();
                Node collider = (Node)result["collider"];
                if (collider is PlayerController){
                    (collider as PlayerController).Rpc("Damage", _damage);
                }
            } else {
                GD.Print("LOCAL HIT");
                Hit((Vector3)result["position"], (Vector3)result["normal"]);
            }
        }

        if (IsInstanceValid(_projectileSkin)) {
            Vector3 direction = (_momentum + GlobalTransform.origin) - _projectileSkin.GlobalTransform.origin;
            direction /= _stepPeriod;
            _projectileSkin.SetNewSpeed(direction);
        }
    }

    //Steps the projectile n times per seconds
    private float _stepTimer = 0;
    private float _stepPeriod = 0.05f;
    private float _aliveTimer = 0;
    public override void _Process(float delta) {
        base._Process(delta);

        _stepTimer += delta;
        if (_stepTimer >= _stepPeriod) {
            _stepTimer -= _stepPeriod;
            Step(_stepPeriod);
        }

        _aliveTimer += delta;
        if (_aliveTimer > _alive) {
            RmProjectileSkin();
            QueueFree();
        }

    }

    private void RmProjectileSkin () {
        if (IsInstanceValid(_projectileSkin)) {
            _projectileSkin.QueueFree();
            _projectileSkin = null;
        }
    }

    //Notifies everyone that the projectile hit a player
    [Remote] public void Hit (Vector3 position, Vector3 normal) {
        if (_hit) return;
        _hit = true;

        Translation = position;

        GD.Print("HIT");


        _projectileSkin.QueueFree();

        AudioStreamPlayer3D hitEffect = (AudioStreamPlayer3D)(ResourceLoader.Load("res://scenes/Weapons/Projectiles/ProjectileImpactFX.tscn") as PackedScene).Instance();
        Lobby.Singleton.AddChild(hitEffect);
        hitEffect.Translation = position;
        hitEffect.LookAt(position + normal, Vector3.Up);

        //TODO: Hit Effect
    }

}
