using Godot;
using System.Collections.Generic;
using System;

public class Lobby : Node {
    [Export] private PackedScene _playerScene;
    [Export] private PackedScene _snapshotController;

    public static Lobby Singleton;
    Godot.Collections.Dictionary myInfo = new Godot.Collections.Dictionary();
    Godot.Collections.Array playerInfoArray = new Godot.Collections.Array();


    public override void _Input(InputEvent @event) {
        base._Input(@event);

        if (@event is InputEventKey) {
            InputEventKey inputEventKey = (InputEventKey)@event;

            if (inputEventKey.Pressed && inputEventKey.Scancode == (int)KeyList.P) {
                Host(7777);
            }

            if (inputEventKey.Pressed && inputEventKey.Scancode == (int)KeyList.O) {
                Godot.Collections.Dictionary info = new Godot.Collections.Dictionary();
                info.Add("name", "romimap");
                Connect("92.167.84.110", 7777, info);
            }
        }
    }


    // Called when the node enters the scene tree for the first time.
    public override void _Ready () {
        Singleton = this;

        GetTree().Connect("network_peer_connected", this, "PlayerConnected");
        GetTree().Connect("network_peer_disconnected", this, "PlayerDisconnected");
        GetTree().Connect("connected_to_server", this, "ConnectedOk");
        GetTree().Connect("connection_failed", this, "ConnectedFail");
        GetTree().Connect("server_disconnected", this, "ServerDisconnected");
    }


    public void Host (int port) {
        NetworkedMultiplayerENet networkedMultiplayerPeer = new NetworkedMultiplayerENet();
        Error error = networkedMultiplayerPeer.CreateServer(port);
        GD.Print("HOST !");
        if (error == Error.Ok) {
            GD.Print("OK !");
            GetTree().NetworkPeer = networkedMultiplayerPeer;
            Node snapshotController = _snapshotController.Instance();
            AddChild(snapshotController);
        }
        else
            GD.Print("ERROR !");
    }


    public void Connect (string ip, int port, Godot.Collections.Dictionary info) {
        myInfo = info;
        NetworkedMultiplayerENet networkedMultiplayerPeer = new NetworkedMultiplayerENet();
        Error error = networkedMultiplayerPeer.CreateClient(ip, port);
        GD.Print("CONNECT !");
        if (error == Error.Ok) {
            GD.Print("OK !");
            GetTree().NetworkPeer = networkedMultiplayerPeer;
            myInfo = info;
        }
        else
            GD.Print("ERROR !");
    }

    [Remote] public void RespawnPlayer (int playerId, Godot.Collections.Array playerStateNetwork) {
        Node playerNode = GetNode("" + playerId);
        if (playerNode != null) {
            PlayerController.PlayerControllers.Remove((PlayerController)playerNode);
            playerNode.Name = "Removed";
            playerNode.QueueFree();
        }

        PlayerController.PlayerState playerState = new PlayerController.PlayerState(playerStateNetwork);
        playerState.health = 100;
        PlayerController player = (PlayerController)_playerScene.Instance();
        player.Name = "" + playerId;
        player.ID = playerId;
        player.SetNetworkMaster(playerId);
        AddChild(player);

        player.SetState(playerState);

        if (GetTree().IsNetworkServer())
            player.Transform = playerState.transform;
        else
            player.ClientSideInputHandler.Teleport(playerState.transform.origin);
        


        GD.Print("SPAWN " + player.Name + " @ " + player.Transform.origin);
    }


    [Remote] public void SendInfoToServer (Godot.Collections.Dictionary info) {
        int id = GetTree().GetRpcSenderId();
        info.Add("id", id);
        //TODO: info verification

        //Spawn the player to everyone
        PlayerController.PlayerState state = new PlayerController.PlayerState();
        state.health = 100;
        Spatial spawn = Lobby.Singleton.GetSpawnPosition();
        state.transform = spawn.Transform;
        Lobby.Singleton.Rpc("RespawnPlayer", id, state.Serialize());
        RespawnPlayer(id, state.Serialize());


        //Spawn everyone to the player
        foreach (Godot.Collections.Dictionary playerInfo in playerInfoArray) {
            GD.Print("Setting up player " + (int)playerInfo["id"]);
            PlayerController player = (PlayerController)GetNode("" + (int)playerInfo["id"]);
            PlayerController.PlayerState playerState = player.GetState();
            RpcId(id, "RespawnPlayer", (int)playerInfo["id"], playerState.Serialize());
        }

        //TODO: info sync

        playerInfoArray.Add(info);
    }

    public Spatial GetSpawnPosition () {
        Godot.Collections.Array spawnPoints = GetNode("Spawn").GetChildren();
        Random r = new Random();
        return (Spatial)spawnPoints[r.Next(spawnPoints.Count)];
    }

    //Called on all peers (server included)
    public void PlayerConnected (int id) {
        
    }


    //Called on all peers (server included)
    public void PlayerDisconnected (int id) {
        foreach (Godot.Collections.Dictionary playerInfo in playerInfoArray) {
            if (id == (int)playerInfo["id"]) {
                playerInfoArray.Remove(playerInfo);
                //break;
            }
        }
        PlayerController.PlayerControllers.Remove((PlayerController)GetNode("" + id));
        GetNode("" + id).QueueFree();
    }


    //On Connexion OK
    public void ConnectedOk () {
            RpcId(1, "SendInfoToServer", myInfo);
    }


    //On Connexion Fail
    public void ConnectedFail () {

    }


    //On Kick
    public void ServerDisconnected () {

    }
}
