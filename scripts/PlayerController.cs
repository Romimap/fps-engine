using Godot;
using System.Collections.Generic;
using System;

//Handles the player state and actions
public class PlayerController : KinematicBody {
    public struct PlayerState {
        public Transform transform;
        public Vector3 momentum;
        public float azimuth;
        public float inclinaison;
        public float health;
        
        public PlayerState (PlayerState a, PlayerState b, float t) {
            Transform transform = new Transform(); 
            Vector3 origin = a.transform.origin * t + b.transform.origin * (1 - t);
            Basis basis = a.transform.basis.Slerp(b.transform.basis, t);
            
            this.transform = transform;
            this.momentum = a.momentum * t + b.momentum * (1 - t);
            this.azimuth = a.azimuth * t + b.azimuth * (1 - t);
            this.inclinaison = a.inclinaison * t + b.inclinaison * (1 - t);
            this.health = (int)(a.health * t + b.health * (1 - t));
        }

        public PlayerState (Godot.Collections.Array playerStateNetwork) {
            this.transform = (Transform)playerStateNetwork[0];
            this.momentum = (Vector3)playerStateNetwork[1];
            this.azimuth = (float)playerStateNetwork[2];
            this.inclinaison = (float)playerStateNetwork[3];
            this.health = (float)playerStateNetwork[4];
        }

        public Godot.Collections.Array Serialize () {
            Godot.Collections.Array playerStateNetwork = new Godot.Collections.Array();
            playerStateNetwork.Add(transform);
            playerStateNetwork.Add(momentum);
            playerStateNetwork.Add(azimuth);
            playerStateNetwork.Add(inclinaison);
            playerStateNetwork.Add(health);

            return playerStateNetwork;
        }
    }

    public int ID = 0;
    public Vector3 _momentum = new Vector3();
    public float CurrentSpeed {get{return _momentum.Length();}}
    public float CurrentSpeedNormalized {
        get{
            float l =  _momentum.Length();

            float s = l / _speed;
            if (s > 1) s = 1;
            if (s < 0) s = 0;

            return s;
            }
        }
    public Vector3 CurrentDirection {
        get {
            Vector3 ans;

            Spatial direction = (Spatial)GetNode("Azimuth");

            ans.x = Mathf.Abs(direction.Transform.basis.x.AngleTo(_momentum) / 3.1415f) * 2 - 1;
            ans.y = Mathf.Abs(direction.Transform.basis.y.AngleTo(_momentum) / 3.1415f) * 2 - 1;
            ans.z = Mathf.Abs(direction.Transform.basis.z.AngleTo(_momentum) / 3.1415f) * 2 - 1;

            return ans;
        }
    }
    private float _speed = 300f;
    private float _acceleration = 0.2f;
    private float _jumpForce = 800;
    private float _gravity = -100f;
    private float _azimuth = 0;
    private float _inclinaison = 0;
    public float _health = 100;
    public bool _dead = false;
    public bool Dead {get{return _dead;}}
    private RichTextLabel _healthText;
    [Export] private PackedScene _stepSoundScene;

    public float Azimuth {
        get {return _azimuth;}
        set {
            _azimuth = value;
            (GetNode("Azimuth") as Spatial).RotationDegrees = new Vector3(0, _azimuth, 0);
        }
    }
    public float Inclinaison {
        get {return _inclinaison;}
        set {
            _inclinaison = value;
            if (_inclinaison > 90) _inclinaison = 90;
            if (_inclinaison < -90) _inclinaison = -90;
            (GetNode("Azimuth/Inclinaison") as Spatial).RotationDegrees = new Vector3(_inclinaison, 0, 0);
        }
    }

    private static List<PlayerController> _playerControllers = new List<PlayerController>();
    public static List<PlayerController> PlayerControllers {get{return _playerControllers;}}

    private ClientSideInputHandler _clientSideInputHandler;
    public ClientSideInputHandler ClientSideInputHandler {get{return _clientSideInputHandler;}}
    private ServerSideInputHandler _serverSideInputHandler;
    public ServerSideInputHandler ServerSideInputHandler {get{return _serverSideInputHandler;}}

    public PlayerState GetState () {
        PlayerState ans;
        ans.transform = Transform;
        ans.momentum = _momentum;
        ans.azimuth = _azimuth;
        ans.inclinaison = _inclinaison;
        ans.health = _health;

        return  ans; 
    }

    public void SetState (PlayerState state) {
        Transform = state.transform;
        _momentum = state.momentum;
        if (!IsNetworkMaster()) { //The network master sets it on look direction
            Azimuth = state.azimuth;
            Inclinaison = state.inclinaison;
        }
        _health = state.health;
    }

    
    public void Move (ClientSideInputHandler.InputData input, float delta) {
        if (_dead) return;

        Spatial direction = (Spatial)GetNode("Azimuth");
        Vector3 targetMovement = (direction.Transform.basis.z * input.forward) + (-direction.Transform.basis.x * input.right);
        targetMovement = targetMovement.Normalized();
        targetMovement *= _speed;
        
        float y = _momentum.y;
        _momentum = targetMovement * _acceleration + _momentum * (1f - _acceleration);
        _momentum.y = y;


        Godot.Collections.Array ignore = new Godot.Collections.Array();
        ignore.Add(this);
        PhysicsDirectSpaceState spaceState = GetWorld().DirectSpaceState;
        Godot.Collections.Dictionary result = spaceState.IntersectRay(Translation, Translation + new Vector3(0, -1.05f, 0), ignore);
        bool OnFloor = result.Count > 0;

        if (OnFloor) {
            _momentum.y = 0;
            if (input.jump) {
                _momentum.y = _jumpForce;
            }
        }

        if (!OnFloor) {
            _momentum.y += _gravity;
        }
        
        MoveAndSlide(_momentum * delta, new Vector3(0, 1 ,0), true, 4, 0.78f, true);
    }

    [RemoteSync] public void Damage (int damage) {
        _health  -=  damage;
        GD.Print("DAMAGE ! (" + damage + ", " + _health + ")");
        if (_health <= 0) {
            _health = 0;
            if (GetTree().IsNetworkServer()) {
                Rpc("Die");
            }
        }
        if (IsNetworkMaster()) RefreshHealthUI();
    }

    public void RefreshHealthUI () {
        _healthText.Text = "" + (int)Mathf.Ceil(_health);
    }

    [RemoteSync] public void Die () {
        GD.Print("DEAD :(");
        _dead = true;
        
        if (GetTree().IsNetworkServer()) {
            PlayerState state = GetState();
            Spatial spawn = Lobby.Singleton.GetSpawnPosition();
            state.transform = spawn.Transform;
            Lobby.Singleton.Rpc("RespawnPlayer", ID, state.Serialize());
            Lobby.Singleton.RespawnPlayer(ID, state.Serialize());
        }
    }

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _clientSideInputHandler = (ClientSideInputHandler)GetNode("ClientSideInputHandler");
        _serverSideInputHandler = (ServerSideInputHandler)GetNode("ServerSideInputHandler");
        _healthText = (RichTextLabel)GetNode("/root/World/UI/BottomLeft/Health");
        _playerControllers.Add(this);
        if (IsNetworkMaster()) {
            (GetNode("Azimuth/Inclinaison/Camera") as PlayerCamera).SetCurrent();
            (GetNode("Skin") as Spatial).Visible = false;
            RefreshHealthUI();
        } else {
            (GetNode("Azimuth/Inclinaison/Camera/WeaponSkin") as Spatial).Visible = false;
        }        
    }

    private Vector3 _previousPosition; 
    private float _distance;
    private const float _stepDistance = 2f;
    public override void _Process(float delta) {
        base._Process(delta);

        if (!GetTree().IsNetworkServer()) {
            _distance += (Translation - _previousPosition).Length();
            _previousPosition = Translation;
            if (_distance > _stepDistance) {
                _distance -= _stepDistance;
                AudioStreamPlayer3D stepSound = (AudioStreamPlayer3D)_stepSoundScene.Instance();
                Random r = new Random();
                stepSound.Stream = (AudioStreamSample)ResourceLoader.Load("res://sound/stepdirt_" + (r.Next(8) + 1) + ".wav");
                AddChild(stepSound);
            }
        }

    }
}
