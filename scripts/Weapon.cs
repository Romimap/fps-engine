using Godot;
using System;

public class Weapon : Node {
    //TODO: Weapon & Projectile stat
    public struct WeaponState {
        public float magazine;
        public uint bulletId;
        public uint lastBulletId;

        public WeaponState (Godot.Collections.Array weaponStateNetwork) {
            magazine = (float)weaponStateNetwork[0];
            bulletId = Convert.ToUInt32(weaponStateNetwork[1]);
            lastBulletId = Convert.ToUInt32(weaponStateNetwork[2]);
        }

        public Godot.Collections.Array Serialize () {
            Godot.Collections.Array ans = new Godot.Collections.Array();

            ans.Add(magazine);
            ans.Add(bulletId);
            ans.Add(lastBulletId);

            return ans;
        }
    }

    public struct WeaponValues {
        public float shootPeriod;
        public float reloadPeriod;
        public float bullets;
        public float spread;
        public float capacity;
        public float recoil;

        public WeaponValues (Godot.Collections.Array weaponValues) {
            shootPeriod = (float)weaponValues[0];
            reloadPeriod = (float)weaponValues[1];
            bullets = (float)weaponValues[2];
            spread = (float)weaponValues[3];
            capacity = (float)weaponValues[4];
            recoil = (float)weaponValues[5];
        }

        public Godot.Collections.Array Serialize () {
            Godot.Collections.Array ans = new Godot.Collections.Array();

            ans.Add(shootPeriod);
            ans.Add(reloadPeriod);
            ans.Add(bullets);
            ans.Add(spread);
            ans.Add(capacity);
            ans.Add(recoil);

            return ans;
        }
    }

    private PlayerController _playerController;
    private Spatial _weaponInclinaisonNode;
    private float _shootPeriod = 1.3f;
    private float _shootTimer;
    private float _reloadPeriod = 2.4f;
    private float _reloadTimer;
    private float _bullets = 1;
    private float _spread = Mathf.Pi / 128f; 
    private float _capacity = 30;
    private float _recoil = 0.3f;
    private float _recoilRecover = 1f;
    private float _magazine;
    private AnimationPlayer _weaponAnimationPlayer;
    
    private const float _maxRecoil = Mathf.Pi / 12;
    [Export] private Curve _recoilCurve = new Curve();
    [Export] private Curve _spreadCurve = new Curve();
    [Export] private Curve _runningInaccuracyCurve = new Curve();
    [Export] private PackedScene _projectile;
    private RichTextLabel _magazineText;

    private uint _lastBulletId = 0;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _playerController = (PlayerController)GetParent();
        _weaponInclinaisonNode = (Spatial)_playerController.GetNode("Azimuth/Inclinaison/WeaponInclinaison");
        _magazineText = (RichTextLabel)GetNode("/root/World/UI/BottomRight/Magazine");

        _weaponAnimationPlayer = (AnimationPlayer)_playerController.GetNode("Azimuth/Inclinaison/Camera/WeaponSkin/AnimationPlayer");

        _magazine = _capacity;

        _toShootTimer = _shootPeriod;
        _shootTimer = _shootPeriod;
        _reloadTimer = _reloadPeriod;
    }

    public WeaponState GetState () {
        WeaponState weaponState;

        weaponState.magazine = _magazine;
        weaponState.bulletId = _bulletId;
        weaponState.lastBulletId = _lastBulletId;

        return weaponState;
    }

    public void SetState (WeaponState weaponState) {
        _magazine = weaponState.magazine;
        _bulletId = weaponState.bulletId;
        _lastBulletId = weaponState.lastBulletId;
    }

    private float _toShootTimer;
    private float _toShoot = 0;
    private uint _bulletId = 1;
    public override void _Process(float delta){
        base._Process(delta);

        _toShootTimer += delta;

        if (_toShoot == 0 && _toShootTimer >= _shootPeriod) {
            _toShootTimer = _shootPeriod;
        }

        if (_toShoot > 0 && _toShootTimer >= _shootPeriod) {
            Shoot(_bulletId);
            _bulletId++;
            _toShootTimer -= _shootPeriod;
        }
    }

    private float _currentRecoil = 0;
    public void Step (uint inputID, bool shoot, bool reload, float delta) {
        
        //Timers
        delta /= 1000f;
        _shootTimer += delta;
        _reloadTimer += delta;
        if (_shootTimer > _shootPeriod && !shoot) {
            _shootTimer = _shootPeriod;
        }
        if (_reloadTimer > _reloadPeriod) {
            _reloadTimer = _reloadPeriod;
        }

        //Reload
        if (reload && _reloadTimer >= _reloadPeriod && _magazine < _capacity) { //If we pressed the reload key AND the weapon isn't reloading AND the magazine isn't full
            Reload();
        }

        if (_reloadTimer >= _reloadPeriod && _magazine <= 0) { //If the magazine is empty and we are not already reloading
            Reload();
        }

        //Recoil
        _currentRecoil -= delta * _recoilRecover;
        if (_currentRecoil > 1) _currentRecoil = 1;
        if (_currentRecoil < 0) _currentRecoil = 0;
        _weaponInclinaisonNode.Rotation = Vector3.Zero;
        _weaponInclinaisonNode.Transform = _weaponInclinaisonNode.Transform.Rotated(new Vector3(-1, 0, 0), _recoilCurve.Interpolate(_currentRecoil) * _maxRecoil);

        //Shoot
        if (_reloadTimer >= _reloadPeriod && _shootTimer >= _shootPeriod) {
            if (shoot) {
                _shootTimer -= _shootPeriod;
                _toShoot++;
            }
        }
    }

    public void SetWeaponStateOthers (WeaponState weaponState) {
        _toShoot = weaponState.bulletId - _bulletId;
    }

    private void Shoot (uint bulletId) {
        if (bulletId <= _lastBulletId) return;

        _lastBulletId = bulletId;

        _toShoot--;

        if (IsNetworkMaster() || GetTree().IsNetworkServer()) {
            GD.Print("SHOOT: speed " + _playerController.CurrentSpeedNormalized + " & recoil " + _currentRecoil);
            
            Projectile projectile = new Projectile();
            projectile.CasterID = (uint)_playerController.ID;

            //ProjectileState
            Projectile.ProjectileState projectileState;
            projectileState.ID = bulletId;
            projectileState.casterID = (uint)_playerController.ID;
            projectileState.changeInMomentumGlobal = new Vector3(0, -0.9f, 0);
            projectileState.changeInMomentumLocal = new Vector3(0, 0, 0);
            projectileState.alive = 2;
            projectileState.speed = 10f;

            //Recoil
            if (_currentRecoil > 1) _currentRecoil = 1;
            if (_currentRecoil < 0) _currentRecoil = 0;
            _weaponInclinaisonNode.Rotation = Vector3.Zero;
            _weaponInclinaisonNode.Transform = _weaponInclinaisonNode.Transform.Rotated(new Vector3(-1, 0, 0), _recoilCurve.Interpolate(_currentRecoil) * _maxRecoil);
            _weaponInclinaisonNode.Transform = _weaponInclinaisonNode.Transform.Rotated(new Vector3(1, 0, 0), (float)GD.RandRange(-1, 1) * (_spreadCurve.Interpolate(_currentRecoil) * _spread + _runningInaccuracyCurve.Interpolate(_playerController.CurrentSpeedNormalized) * _spread));
            _weaponInclinaisonNode.Transform = _weaponInclinaisonNode.Transform.Rotated(new Vector3(0, 1, 0), (float)GD.RandRange(-1, 1) * (_spreadCurve.Interpolate(_currentRecoil) * _spread + _runningInaccuracyCurve.Interpolate(_playerController.CurrentSpeedNormalized) * _spread));
            _currentRecoil += _recoil;

            //Momentum
            projectileState.momentum = _weaponInclinaisonNode.GlobalTransform.basis.z * projectileState.speed;

            projectile.SetState(projectileState);
            projectile.Name = "P" + bulletId;
            projectile.Translation = _weaponInclinaisonNode.GlobalTransform.origin;

            GD.Print("SHOOT: " + projectile.Name);

            Lobby.Singleton.AddChild(projectile);

            //Ammo
            _magazine--;
        }

        if (IsNetworkMaster()) {
            //UI
            _magazineText.Text = "" + _magazine;
        }

        //Animation
        _weaponAnimationPlayer.Stop(true);
        _weaponAnimationPlayer.Play("Shoot");
    }

    private void Reload() {
        _reloadTimer = 0;
        _magazine = _capacity;
        //UI
        _magazineText.Text = "" + _magazine;

        //Animation
        _weaponAnimationPlayer.Stop(true);
        _weaponAnimationPlayer.Play("Reload");
    }
}
