using Godot;
using System;
using System.Collections.Generic;

//Recieves input, sends snapshots
public class ServerSideInputHandler : Node {
    //CONFIG
    private const uint _maxBufferedInputs = 100; //Less could reduce stuttering in case of a laggy connection, but also makes client prediction less reliable
    private const uint _maxBufferedPlayerStates = 50;
    private const int _maxInputDeviation = 10;

    private ClientSideInputHandler _clientSideInputHandler;
    private PlayerController _playerController;
    private Weapon _weapon;
    private uint _lastProcessedInputID = 0;
    private List<ClientSideInputHandler.InputData> _inputDatas = new List<ClientSideInputHandler.InputData>();
    public List<PlayerController.PlayerState> PlayerStates = new List<PlayerController.PlayerState>();
    public uint LastSnapshotProcessedByClient {get{return _lastSnapshotProcessedByClient;}}
    private uint _lastSnapshotProcessedByClient = 0;
    public float LastSnapshotProcessedDelta {get{return _lastSnapshotProcessedDelta;}}
    private float _lastSnapshotProcessedDelta = 0f;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
        _clientSideInputHandler = (ClientSideInputHandler)GetNode("../ClientSideInputHandler");
        _weapon = (Weapon)GetNode("../Weapon");
        _playerController = (PlayerController)GetParent();
    }

    [Remote] public void SendInputToServer (uint inputID, Godot.Collections.Array inputDataNetwork) {
        if (!GetTree().IsNetworkServer()) return;
        if (_input < 0) return;
        if (_playerController.Dead) return; //If the player is dead, we dont process inputs from this player anymore
        
        //GD.Print("INPUT IN: " + inputDataNetwork);

        ClientSideInputHandler.InputData inputData = new ClientSideInputHandler.InputData(inputDataNetwork);

        //Reset the buffer if we waited for too long. lastProcessedInputID is set to inputData.id - 1 so inputData is next
        if (_inputDatas.Count >= _maxBufferedInputs) {
            _inputDatas.Clear();
            _lastProcessedInputID = inputData.id - 1;
            GD.Print("Skipped some inputs");
        }

        _inputDatas.Add(inputData);
        _inputDatas.Sort(ClientSideInputHandler.InputData.Compare);

        while (_inputDatas.Count > 0 && _inputDatas[_inputDatas.Count - 1].id == _lastProcessedInputID + 1) {
            _playerController.Azimuth = _inputDatas[_inputDatas.Count - 1].azimuth;
            _playerController.Inclinaison = _inputDatas[_inputDatas.Count - 1].inclinaison;
            _playerController.Move(_inputDatas[_inputDatas.Count - 1], (float)ClientSideInputHandler.InputPeriod / 1000f);
            _lastSnapshotProcessedByClient = _inputDatas[_inputDatas.Count - 1].snapshotID;
            _lastSnapshotProcessedDelta = _inputDatas[_inputDatas.Count - 1].delta;
            _inputDatas.RemoveAt(_inputDatas.Count - 1);
            _lastProcessedInputID++;
        }

        _weapon.Step(inputID, inputData.fire, inputData.reload, ClientSideInputHandler.InputPeriod);

        _input++;
    }

    public void Snapshot () {
        uint snapshotID = SnapshotController.Singleton.CurrentSnapshot;
        uint lastInputID = _lastProcessedInputID;

        PlayerController.PlayerState playerState = _playerController.GetState();
        Godot.Collections.Array playerStateNetwork = playerState.Serialize();
        Godot.Collections.Array weaponStateNetwork = _weapon.GetState().Serialize();

        PlayerStates.Add(playerState);
        if (PlayerStates.Count > _maxBufferedPlayerStates) PlayerStates.RemoveAt(0);


        _clientSideInputHandler.Rpc("SendSnapshotToClient",lastInputID, snapshotID, playerStateNetwork, weaponStateNetwork);
    }

    public float _timer;
    public int _input = 1;
    public override void _Process(float delta) {
        base._Process(delta);

        //Estimates the number of inputs that the client can send us
        //Each inputperiod, we add an available input, if _input goes negative, the player sent too much inputs in too litle time.
        _timer += delta;
        if (_timer >= ClientSideInputHandler.InputPeriod) {
            _timer -= ClientSideInputHandler.InputPeriod;
            _input++;
            if (_input > _maxInputDeviation) {
                _input = _maxInputDeviation;
            }
        }
       
    }
}
